package de.dreamit.example.mngm;

import com.netflix.hystrix.HystrixCollapserMetrics;
import com.netflix.hystrix.HystrixCommandMetrics;
import com.netflix.hystrix.HystrixThreadPoolMetrics;
import com.netflix.hystrix.serial.SerialHystrixDashboardData;
import de.dreamit.example.SenderService;
import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.OutboundEvent;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Logger;

@Named
@Stateless
@Path("/_mgmt")
public class ManagementResource {

    private static Logger log = Logger.getLogger(ManagementResource.class.getName());

    @Inject
    private SenderService senderService;

    @GET
    @Produces("text/event-stream")
    @Path("hystrix.stream")
    public EventOutput hystrixVertxStream() {
        final EventOutput eventOutput = new EventOutput();

        CompletableFuture.runAsync(() -> {
            try {
                HystrixCommandMetrics.getInstances().stream().map(SerialHystrixDashboardData::toJsonString)
                        .forEach(d -> sendServerDataEvent(eventOutput, d));
                HystrixThreadPoolMetrics.getInstances().stream().map(SerialHystrixDashboardData::toJsonString)
                        .forEach(d -> sendServerDataEvent(eventOutput, d));
                HystrixCollapserMetrics.getInstances().stream().map(SerialHystrixDashboardData::toJsonString)
                        .forEach(d -> sendServerDataEvent(eventOutput, d));
            } finally {
                try {
                    eventOutput.close();
                } catch (IOException ioClose) {
                    log.warning("Error closing event output");
                }
            }
        });

        return eventOutput;
    }

    private void sendServerDataEvent(EventOutput eventOutput, String data) {
        try {
            final OutboundEvent.Builder eventBuilder = new OutboundEvent.Builder();
            eventBuilder.data(String.class, data);
            final OutboundEvent event = eventBuilder.build();
            eventOutput.write(event);
        } catch (IOException e) {
            log.warning("Error when writing the event" + e.getMessage());
        }
    }
}
