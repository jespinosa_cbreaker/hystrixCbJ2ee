package de.dreamit.example;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Optional;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * @author Alejandro Garcia
 */
public class HystrixCommandTrackingService extends HystrixCommand<RecordMetadata> {

    private static final Logger logger = Logger.getLogger(HystrixCommandTrackingService.class.getSimpleName());

    private Producer<String, String> producer;
    private String topic;
    private String value;

    //TODO: Add the proper configuration. Maybe this information could come from the admin
    //TODO: Also very important the configration for the Thread pool, depending on the needed capacity
    protected HystrixCommandTrackingService(String topic, String value, Producer<String, String> producer) {
        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("TrackingServiceGroup"))
                .andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
                        .withExecutionTimeoutInMilliseconds(1000)
                        .withCircuitBreakerSleepWindowInMilliseconds(5000)
                        .withCircuitBreakerRequestVolumeThreshold(20)
                        .withCircuitBreakerErrorThresholdPercentage(50)
                        .withMetricsRollingStatisticalWindowInMilliseconds(10000)
                ));
        this.producer = producer;
        this.topic = topic;
        this.value = value;
    }

    @Override
    protected RecordMetadata run() throws Exception {
        final ProducerRecord<String, String> kafkaRecord = new ProducerRecord<>(topic, value);
        final Future<RecordMetadata> result = producer.send(kafkaRecord, (metadata, exception) ->
                Optional.ofNullable(exception).ifPresent(ex -> logger.warning(String.format("Sending tracking data failed. Topic: %s. Value: %s. Error: %s",
                        topic, value, ex.getMessage())))
        );
        return result.get(200, TimeUnit.MILLISECONDS);
    }

    //TODO: The fallback can be disabled in the configuration but it means that all exception will raised and you should catch them explicity in the call to the Hystrix command.
    @Override
    protected RecordMetadata getFallback() {
        return null;
    }
}