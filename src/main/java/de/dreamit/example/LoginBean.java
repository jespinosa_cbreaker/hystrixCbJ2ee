package de.dreamit.example;


import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;

@Named
@Stateless
@Path("/api")
public class LoginBean {

    public static final String PRODUCED_TYPE_JSON = MediaType.APPLICATION_JSON + ";charset=utf-8";

    @Inject
    private SenderService senderService;

    @GET
    @Path("/test")
    @Produces({PRODUCED_TYPE_JSON})
    public Response doLogin(@QueryParam("user") String user, @QueryParam("pass") String pass) {
        String json = "{\"number\": " + senderService.getNumber() + ", \"user\": \"" + user + "\", \"time\": \"" + new Date().getTime() + "\"}";
        senderService.produce(json);
        Response.ResponseBuilder builder = Response.ok(json);
        return builder.build();
    }

}
