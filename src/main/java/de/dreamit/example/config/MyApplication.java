package de.dreamit.example.config;

import de.dreamit.example.LoginBean;
import de.dreamit.example.mngm.ManagementResource;

import javax.ws.rs.ApplicationPath;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/")
public class MyApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> classes = new HashSet<>();
        classes.add(LoginBean.class);
        classes.add(ManagementResource.class);
        return classes;
    }
}
