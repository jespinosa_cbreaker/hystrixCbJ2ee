package de.dreamit.example;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.config.ConfigException;

import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named
@ApplicationScoped
public class SenderService {

    private static final Logger logger = Logger.getLogger(SenderService.class.getSimpleName());

    private Producer<String, String> producer;

    private AtomicInteger number = new AtomicInteger();

    public SenderService() {
        createProducer();
    }

    public void produce(String value) {
        if(producer != null) {
            String topic = "tracking";
            new HystrixCommandTrackingService(topic, value, producer).queue();
        }
    }

    private void createProducer() {
        close();
        try {
            producer = new KafkaProducer<>(getProperties());
        } catch (ConfigException e) {
            logger.log(Level.SEVERE, "Error configuring kafka producer: " + e.getMessage());
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Error creating kafka producer: " + e.getMessage());
        }
    }

    private Properties getProperties() {
        Properties params = new Properties();
        params.put("bootstrap.servers", "localhost:9092");
        params.put("acks", "0");
        params.put("retries", 0);
        params.put("batch.size", 16384);
        params.put("linger.ms", 1);
        params.put("buffer.memory", 33554432);
        params.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        params.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        params.put("max.block.ms", 300L);
        params.put("metadata.fetch.timeout.ms", 300L);
        params.put("metadata.max.age.ms", 300000L);
        return params;
    }


    @PreDestroy
    void close() {
        if (producer != null) {
            try {
                producer.close();
            } catch (Exception e) {
                logger.warning("Error closing producer: " + e.getMessage());
            }
        }
    }

    public Integer getNumber() {
        return number.incrementAndGet();
    }
}
